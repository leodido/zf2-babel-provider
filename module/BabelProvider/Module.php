<?php
/**
 * babel-provider
 *
 * @link        ...
 *
 * @copyright   ...
 *
 * @license     ...
 */
namespace BabelProvider;

use Zend\Console\Adapter\AdapterInterface;
use Zend\ModuleManager\Feature\ConsoleBannerProviderInterface;
use Zend\ModuleManager\Feature\ConsoleUsageProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

/**
 * Class Module
 */
class Module implements ConsoleBannerProviderInterface, ConsoleUsageProviderInterface
{
    /**
     * @param MvcEvent $evt
     */
    public function onBootstrap(MvcEvent $evt)
    {
        $eventManager = $evt->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ],
            ],
        ];
    }

    /**
     * @param AdapterInterface $console
     * @return null|string
     */
    public function getConsoleBanner(AdapterInterface $console)
    {
        $ncols = $console->getWidth();
        $separ = str_repeat('=', $ncols);
        $banner = 'BABELNET IMPORTER';
        return $separ . PHP_EOL . $banner . PHP_EOL . $separ;
    }

    /**
     * @param AdapterInterface $console
     * @return array|null|string
     */
    public function getConsoleUsage(AdapterInterface $console)
    {
        $console->clearScreen();
        return [
            'test' => 'Test the MongoDB connection',
            'provide' => 'Store the path of BabelNet dump files',
            [
                '--dump=...',
                'Path of the core dump file, relative to the project root directory or absolute.'
            ],
            [
                '[--glosses=...]',
                'Path of the glosses dump file, relative to the project root directory or absolute (optional).'
            ],
            'import' => 'Store the BabelNet dump (and optionally the glosses, if provided) into MongoDB',
            'subset' => 'Extract a domain from BabelNet and store it into a new MongoDB collection',
            [
                '--word=...',
                'The root word of the BabelNet domain.'
            ],
            [
                '--collection=...',
                'The name of the MongoDB collection.'
            ],
            [
                '[--depth=...]',
                'The depth up to which to navigate the tree. Value 0 means max depth [default].'
            ],
            [
                '[--roots=...]',
                'A list of identifier (with or without the BabelNet prefix) indicating other roots of the domain'
            ],
            [
                '[--verbose]',
                'A flag indicating whether the command have to be verbose or not [default not]'
            ],
            'index' => 'Store the BabelNet senses into Sphinx Search indexes',
            [
                '--languages=...',
                'Comma separated languages for which the senses have to be indexed'
            ],
            'create source' => 'Write the BabelNet senses in Sphinx Search XML data source format to an output device',
            [
                '--languages=...',
                'Comma separated languages for which the senses have to be indexed'
            ],
            [
                '[--stdout]',
                'A flag indicating whether to write the senses to stdout [default not]'
            ],
            [
                '[--verbose]',
                'A flag indicating whether the command have to be verbose or not [default not]'
            ]
        ];
    }
}
