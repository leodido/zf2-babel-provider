<?php
/**
 * babel-provider
 *
 * @link        ...
 *
 * @copyright   ...
 *
 * @license     ...
 */

namespace BabelProvider\Filter;

use Zend\Filter\FilterInterface;
use Zend\Filter\Exception\RuntimeException;

/**
 * Class AbbreviatedNumber
 */
class AbbreviatedNumber implements FilterInterface
{
    /**
     * @var array
     */
    private $abbreviations = [12 => 'T', 9 => 'B', 6 => 'M', 3 => 'K', 0 => ''];

    /**
     * @var
     */
    private $rounded;

    /**
     * @return mixed
     */
    public function isRounded()
    {
        return $this->rounded;
    }

    /**
     * @param bool $rounded
     */
    public function __construct($rounded = false)
    {
        $this->rounded = $rounded;
    }

    /**
     * @return array
     */
    public function getAbbreviations()
    {
        return $this->abbreviations;
    }

    /**
     * @param array $abbreviations
     */
    public function setAbbreviations($abbreviations)
    {
        $this->abbreviations = $abbreviations;
    }

    /**
     * Returns the result of filtering $value
     *
     * @param  mixed $value
     * @throws RuntimeException If filtering $value is not a number or a numeric strings
     * @return mixed
     */
    public function filter($value)
    {
        if (!is_numeric($value)) {
            $msg = "This filter works only for number or numeric strings.";
            throw new RuntimeException($msg);
        }

        $result = $value;
        foreach ($this->getAbbreviations() as $exponent => $abbreviation) {
            if ($value >= pow(10, $exponent)) {
                $result = floatval($value / pow(10, $exponent));

                return ($this->isRounded() ? round($result, 1) : $result) . $abbreviation;
            }
        }

        return $result;
    }
}
