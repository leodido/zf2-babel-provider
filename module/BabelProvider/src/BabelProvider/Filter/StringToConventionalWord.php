<?php
/**
 * babel-provider
 *
 * @link        ...
 *
 * @copyright   ...
 *
 * @license     ...
 */
namespace BabelProvider\Filter;

use Zend\Filter\AbstractFilter;
use Zend\Filter\Word\SeparatorToSeparator;

/**
 * Class StringToConventionalWord
 */
class StringToConventionalWord extends AbstractFilter
{
    /**
     * @var array
     */
    protected $unavoidedChars = [];

    /**
     * @var array
     */
    private $defaultChars = [' ', '?', ';', ':', '\\', '/', '(', ')', '%', '&', '$', '£', '-', '_', '=', '*', '+'];

    /**
     *
     */
    public function __construct($unavoided = null)
    {
        $unavoidedChars = $this->defaultChars;
        if (!is_null($unavoided)) {
            $unavoidedChars = array_unique(array_merge($unavoidedChars, $unavoided));
        }
        $this->setUnavoidedChars($unavoidedChars);
    }

    /**
     * {@inheritdoc}
     */
    public function filter($value)
    {
        return array_reduce(
            $this->getUnavoidedChars(),
            function ($carry, $item) {
                $filter = new SeparatorToSeparator($item, '');
                return $filter->filter($carry);
            },
            strtolower($value)
        );
    }

    /**
     * @return array
     */
    public function getUnavoidedChars()
    {
        return $this->unavoidedChars;
    }

    /**
     * @param array $unavoidedChars
     */
    public function setUnavoidedChars($unavoidedChars)
    {
        $this->unavoidedChars = $unavoidedChars;
    }
}
