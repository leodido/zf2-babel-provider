<?php
/**
 * babel-provider
 *
 * @link        ...
 *
 * @copyright   ...
 *
 * @license     ...
 */

namespace BabelProvider\Controller\Cli;

use Zend\Console\Adapter\AdapterInterface as Console;
use Zend\Console\ColorInterface;
use Zend\Console\Exception\RuntimeException as ConsoleRuntimeException;
use Zend\Db\Adapter\Exception\RuntimeException;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;

/**
 * Class CliTrait
 */
trait CliTrait
{
    /**
     * @param string $text
     * @param int $color
     * @param null $console
     * @param string $sep
     */
    public function writeHead($text, $color = ColorInterface::GREEN, $console = null, $sep = '=')
    {
        if ($console === null) {
            $console = $this->getConsole();
        }
        $this->writeSeparator($sep, $color, $console);
        $console->writeLine($text);
        $this->writeSeparator($sep, $color, $console);
    }

    /**
     * @return Console
     * @throws \RuntimeException
     */
    public function getConsole()
    {
        $console = $this->getServiceLocator()->get('console');
        if (!$console instanceof Console) {
            throw new ConsoleRuntimeException('Cannot obtain console adapter.');
        }
        return $console;
    }

    /**
     * @param string $sep
     * @param int $color
     * @param null $console
     */
    public function writeSeparator($sep = '=', $color = ColorInterface::GREEN, $console = null)
    {
        if ($console === null) {
            $console = $this->getConsole();
        }
        $console->writeLine(str_repeat($sep, $console->getWidth()), $color);
    }

    /**
     * @param bool $logging
     * @return \MongoDB|null
     */
    public function getMongoConnection($logging = true)
    {
        $console = $this->getConsole();
        try {
            if ($logging) {
                $console->write("> MongoDB ...");
            }
            /** @var $mongodb \MongoDB */
            $mongodb = $this->getServiceLocator()->get('babelnet-default');
            if ($mongodb->w == true) {
                if ($logging) {
                    $console->write("\t\t\tOK\n", ColorInterface::GREEN);
                }

                return $mongodb;
            } elseif ($logging) {
                $console->write("\t\t\tNO\n", ColorInterface::RED);
            }
        } catch (ServiceNotCreatedException $exc) {
            if ($logging) {
                $console->write("\t\t\tNO\n", ColorInterface::RED);
            }
            $console->writeLine(str_repeat("=", $console->getWidth()), ColorInterface::RED);
            $console->writeLine("Message: \"" . $exc->getMessage() . "\".", ColorInterface::RED);
            $console->writeLine(str_repeat("=", $console->getWidth()), ColorInterface::RED);
        } catch (ServiceNotFoundException $exc) {
            if ($logging) {
                $console->write("\t\t\tNO\n", ColorInterface::RED);
            }
            $console->writeLine(str_repeat("=", $console->getWidth()), ColorInterface::RED);
            $console->writeLine("Message: \"" . $exc->getMessage() . "\".", ColorInterface::RED);
            $console->writeLine(str_repeat("=", $console->getWidth()), ColorInterface::RED);
        }

        return null;
    }

    /**
     * @param bool $logging
     * @return null|\Zend\Db\Adapter\Adapter
     */
    public function getSphinxAdapter($logging = true)
    {
        $console = $this->getConsole();
        try {
            if ($logging) {
                $console->write("> SphinxSearch ...");
            }
            /** @var $adapter \Zend\Db\Adapter\Adapter */
            $adapter = $this->getServiceLocator()->get('sphinxql');
            $adapter->getDriver()->getConnection()->connect();
            if ($logging) {
                $console->write("\t\tOK\n", ColorInterface::GREEN);
            }

            return $adapter;
        } catch (ServiceNotCreatedException $exc) {
            if ($logging) {
                $console->write("\t\tNO\n", ColorInterface::RED);
            }
            $console->writeLine(str_repeat("=", $console->getWidth()), ColorInterface::RED);
            $console->writeLine("Message: \"" . $exc->getMessage() . "\".", ColorInterface::RED);
            $console->writeLine(str_repeat("=", $console->getWidth()), ColorInterface::RED);
        } catch (ServiceNotFoundException $exc) {
            if ($logging) {
                $console->write("\t\tNO\n", ColorInterface::RED);
            }
            $console->writeLine(str_repeat("=", $console->getWidth()), ColorInterface::RED);
            $console->writeLine("Message: \"" . $exc->getMessage() . "\".", ColorInterface::RED);
            $console->writeLine(str_repeat("=", $console->getWidth()), ColorInterface::RED);
        } catch (RuntimeException $exc) {
            if ($logging) {
                $console->write("\t\tNO\n", ColorInterface::RED);
            }
            $console->writeLine(str_repeat("=", $console->getWidth()), ColorInterface::RED);
            $console->writeLine("Message: \"" . $exc->getMessage() . "\".", ColorInterface::RED);
            $console->writeLine(str_repeat("=", $console->getWidth()), ColorInterface::RED);
        }

        return null;
    }
}
