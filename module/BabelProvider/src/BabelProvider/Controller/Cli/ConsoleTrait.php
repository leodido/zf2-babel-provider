<?php
/**
 * babel-provider
 *
 * @link        ...
 *
 * @copyright   ...
 *
 * @license     ...
 */
namespace BabelProvider\Controller\Cli;

use Zend\Console\Exception\InvalidArgumentException;

/**
 * Class ConsoleTrait
 */
trait ConsoleTrait
{
    /**
     * @param string $name Parameter name
     * @param int $default (optional) default value in case the parameter does not exist
     * @return int
     * @throws \Zend\Console\Exception\InvalidArgumentException
     */
    public function getIntParam($name, $default = null)
    {
        /* @var $request \Zend\Console\Request */
        $request = $this->getRequest();
        $param = $request->getParam($name, $default);
        if (is_numeric($param)) {
            return (int)$param;
        } elseif ($param === $default) {
            return null;
        } else {
            throw new InvalidArgumentException('Param "' . $name . '" have to be an integer or an integer string.');
        }
    }
}
