<?php
/**
 * babel-provider
 *
 * @link        ...
 *
 * @copyright   ...
 *
 * @license     ...
 */
namespace BabelProvider\Controller\Cli;

use BabelProvider\Controller\Cli\CliTrait;
use BabelProvider\Filter\AbbreviatedNumber;
use BabelProvider\Model\AbstractBabelNetReader;
use BabelProvider\Model\BabelNetCoreReader;
use BabelProvider\Model\BabelNetGlossesReader;
use Zend\Console\ColorInterface;
use Zend\Mvc\Controller\AbstractActionController;

/**
 * Class ImportController
 */
class ImportController extends AbstractActionController
{
    use CliTrait;

    const PROVIDE_COLLECTION = 'source';
    const COREDUMP_COLLECTION = 'graph';
    const GLOSSES_COLLECTION = 'gloss';

    /**
     * @return bool
     */
    public function testAction()
    {
        $this->getMongoConnection();
        $this->getSphinxAdapter();

        return false;
    }

    /**
     * @return bool
     */
    public function provideAction()
    {
        /* @var $request \Zend\Console\Request */
        $request = $this->getRequest();
        // Retrieve parameters
        $core = $request->getParam('dump');
        $glosses = $request->getParam('glosses');
        // Process parameters
        $console = $this->getConsole();
        $dump = realpath($core);
        $gdump = $glosses ? realpath($glosses) : null;
        $obj = null;
        $opts = [
            'fsync' => true
        ];
        if ($dump) {
            if (is_file($dump)) {
                $obj = [
                    '_id' => 1,
                    'dump' => $dump
                ];
            } else {
                $console->writeLine("ERROR: '${dump}' is not a file.", ColorInterface::RED);
            }
        } else {
            $console->writeLine(
                "ERROR: file '${core}' not found into '" . getcwd() . "' directory.",
                ColorInterface::RED
            );
        }
        if ($gdump !== null) {
            if ($gdump) {
                if (is_file($gdump)) {
                    $obj = array_merge($obj, ['glosses' => $gdump]);
                } else {
                    $console->writeLine("WARNING: '${gdump}' is not a file, ignored.", ColorInterface::YELLOW);
                }
            } else {
                $console->writeLine(
                    "WARNING: file '${glosses}' not found into '" . getcwd() . "' directory, ignored.",
                    ColorInterface::YELLOW
                );
            }
        }
        // Upsert
        $console->write("Storing ...");
        if ($obj !== null) {
            /* @var $mongo \MongoDB */
            $mongo = $this->getServiceLocator()->get('babelnet-default');
            $provideCollection = $mongo->selectCollection(self::PROVIDE_COLLECTION);
            if ($provideCollection->count() == 0) {
                $provideCollection->insert($obj, $opts);
            } else {
                $provideCollection->update(['_id' => 1], $obj, $opts);
            }
            $console->write("\tOK\n", ColorInterface::GREEN);
        } else {
            $console->write("\tFAIL\n", ColorInterface::GREEN);
        }

        return false;
    }

    /**
     * @return bool
     */
    public function importAction()
    {
        $console = $this->getConsole();
        // Check core dump file
        /* @var $mongo \MongoDB */
        $mongo = $this->getServiceLocator()->get("babelnet-default");
        $provideCollection = $mongo->selectCollection(self::PROVIDE_COLLECTION);
        $cursor = $provideCollection->find();
        $cursor->rewind();
        $count = $cursor->count();
        $data = null;
        if ($count > 1) {
            $console->writeLine("ERROR: more than one provider found.", ColorInterface::RED);
            return false;
        } else {
            if ($count == 1) {
                $data = $cursor->current();
                $dump = $data["dump"];
                if (is_readable($dump)) {
                    $console->write("Core dump path:\t\t\t", ColorInterface::GREEN);
                    $console->write("${dump}\n");
                } else {
                    $console->writeLine("ERROR: core dump file is not readable.", ColorInterface::RED);
                    return false;
                }
            } else {
                $console->writeLine("ERROR: no provider found.", ColorInterface::RED);
                return false;
            }
        }
        $console->writeLine(str_repeat("=", $console->getWidth()), ColorInterface::GREEN);
        // Process core dump file
        $reader = new BabelNetCoreReader($dump);
        $this->import($reader, self::COREDUMP_COLLECTION, 500000);
        // Check glosses file
        $glosses = null;
        if (isset($data["glosses"]) && is_readable($data["glosses"])) {
            $glosses = $data["glosses"];
            $console->write("Glosses dump path:\t\t", ColorInterface::GREEN);
            $console->write("${data['glosses']}\n");
        } else {
            $console->writeLine("WARNING: glosses dump path not provided (or not readable).", ColorInterface::YELLOW);
            $console->writeLine(str_repeat("=", $console->getWidth()), ColorInterface::YELLOW);
            return false;
        }
        $console->writeLine(str_repeat("=", $console->getWidth()), ColorInterface::GREEN);
        // Process glosses file
        $reader = new BabelNetGlossesReader($glosses);
        $this->import($reader, self::GLOSSES_COLLECTION, 500000);

        return false;
    }

    /**
     * @param AbstractBabelNetReader $reader
     * @param $collection
     * @param int $threshold
     */
    private function import(AbstractBabelNetReader $reader, $collection, $threshold = 1000)
    {
        $console = $this->getConsole();

        /* @var $mongo \MongoDB */
        $mongo = $this->getServiceLocator()->get("babelnet-default");
        $collection = $mongo->selectCollection($collection);
        $console->write("Dropping '" . $collection . "' collection ...\t");
        $collection->drop();
        $console->write("OK\n", ColorInterface::GREEN);
        $console->writeLine(str_repeat("=", $console->getWidth()), ColorInterface::GREEN);
        $options = [
            "w" => 0
        ];

        $start = microtime(true);
        $total = null;
        $counter = 0;
        $num = 0;
        $filter = new AbbreviatedNumber(true);
        foreach ($reader->getLines() as $num => $line) {
            if ($num === ($counter * $threshold) + 1) {
                $console->write("...\t\t\t\t");
            }
            if (($num % $threshold) == 0 && $num > 0) {
                $total = microtime(true) - $start;
                $counter++;
                $console->write(
                    sprintf("%.3f", $total) . " sec\t" . $filter->filter($counter * $threshold) . " rows\n"
                );
            }
            $collection->insert($line, $options);
        }
        $total = microtime(true) - $start;
        $console->write(
            sprintf("%.3f", $total) . " sec\t" . $filter->filter($num - ($counter * $threshold) + 1) . " rows\n"
        );
        $console->writeLine(str_repeat('=', $console->getWidth()), ColorInterface::GREEN);
        $console->writeLine(
            $filter->filter($num) .
            " rows (comments included) processed in " .
            sprintf("%.3f", $total) . " seconds" .
            " (~ " . sprintf("%f", ($total / $num) * 1000) . " ms/row)."
        );
        $console->writeLine(str_repeat("=", $console->getWidth()), ColorInterface::GREEN);
    }
}
