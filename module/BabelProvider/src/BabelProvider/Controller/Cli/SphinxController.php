<?php
/**
 * babel-provider
 *
 * @link        ...
 *
 * @copyright   ...
 *
 * @license     ...
 */
namespace BabelProvider\Controller\Cli;

use BabelProvider\Filter\AbbreviatedNumber;
use BabelProvider\Model\Sphinx\XMLSource2;
use SphinxSearch\Indexer;
use Zend\Console\ColorInterface;
use Zend\Console\Exception\InvalidArgumentException;
use Zend\Console\Request;
use Zend\Dom\Document;
use Zend\I18n\Filter\NumberFormat;
use Zend\Mvc\Controller\AbstractActionController;

/**
 * Class SphinxController
 */
class SphinxController extends AbstractActionController
{
    use CliTrait;

    /**
     * @var array
     */
    private $sourceAttributes = [
        ['name' => 'bnid', 'type' => 'string'],
        ['name' => 'type', 'type' => 'string'],
        ['name' => 'synset', 'type' => 'string'],
        ['name' => 'probability', 'type' => 'float', 'default' => '1.0']
        // default 1.0 ? or more in order to give boost to non translated concept
    ];

    /**
     * @var array
     */
    private $sourceFields = [
        ['name' => 'sense', 'attr' => 'string'],
        ['name' => 'source', 'attr' => 'string'],
        ['name' => 'region', 'attr' => 'string'],
    ];

    /**
     * TODO: add option to handle output format (i.e., XML || TSV)
     * @return bool
     */
    public function createSourceAction()
    {
        $console = $this->getConsole();
        $configs = $this->getServiceLocator()->get('Config');
        // Retrieve parameters
        /* @var $request \Zend\Console\Request */
        $request = $this->getRequest();
        $langs = null;
        try {
            $langs = $this->getLanguages($request);
        } catch (InvalidArgumentException $exc) {
            $this->writeHead('Message: "' . $exc->getMessage() . '".', ColorInterface::RED, $console);
            return false;
        }
        $stdout = $request->getParam('stdout');
        $verbose = $request->getParam('verbose');
        // Retrieve MongoDB
        /** @var $mongodb \MongoDB */
        $mongodb = $this->getMongoConnection(false);
        $graph = null;
        if ($mongodb != null) {
            $graph = $mongodb->selectCollection(ImportController::COREDUMP_COLLECTION);
        }
        if ($graph == null) {
            if ($verbose) {
                $this->writeHead('Collection not found.', ColorInterface::RED, $console);
            }
            return false;
        }
        // Statistics facilties
        $time = 0;
        $nsenses = 0;
        $threshold = 100000;
        $nFilter = new AbbreviatedNumber(true);
        $tFilter = new NumberFormat('en', \NumberFormatter::DURATION);
        // Fetch babelnet documents (by languages) from mongodb
        foreach ($langs as $lan) {
            $start = microtime(true);
            $total = null;
            $num = 0; // number of documents
            $index = 0; // number of senses
            $counter = 0;
            $this->writeHead(
                'Language: ' . strtoupper($configs['languages'][$lan]) . '.',
                ColorInterface::GREEN,
                $console
            );
            //
            $regex = '/^(\\w+)\\:(' . $lan . ')\\:/i';
            $cursor = $this->extractSense($graph, ['senses' => new \MongoRegex($regex)]);
            // Instantiate index source
            $src = new XMLSource2();
            $termSource = new XMLSource2(); // FIXME:?
            if (!$stdout) {
                $uri = 'config/sphinx/' . $configs['sphinx']['indexes']['prefix'] . $lan . '.xml';
                $src->openUri($uri);
                $termSource->openUri('config/sphinx/terms_' . $lan . '.xml'); // FIXME:?
            }
            $src->setFields($this->sourceFields);
            $src->setAttributes($this->sourceAttributes);
            $src->beginOutput();
            $termSource->setFields([['name' => 'term', 'attr' => 'string']]); // FIXME:?
            $termSource->beginOutput(); // FIXME:?
            // Iterate retrieved documents
            foreach ($cursor as $doc) {
                // Filtering senses by current language
                foreach (preg_grep($regex, $doc['senses']) as $sense) {
                    // Log statistics and information
                    if ($verbose) {
                        if ($index == ($counter * $threshold) + 1) {
                            $console->write("...\t\t\t\t\t\t");
                        }
                        if (($index % $threshold) == 0 && $num > 0) {
                            $total = microtime(true) - $start;
                            $counter++;
                            $this->logStats($total, $counter * $threshold, 'senses', $console, $tFilter, $nFilter);
                        }
                    }
                    // Sense extraction
                    $row = $this->createSenseData($sense, $doc, $index + 1);
                    // Write index source
                    $src->addDocument($row);
                    $termSource->addDocument(['id' => $row['id'], 'term' => $row['sense']]); // FIXME:?
                    $index++;
                }
                $num++;
            }
            // Close index source
            $src->endOutput();
            $termSource->endOutput(); // FIXME:?
            $total = microtime(true) - $start;
            if ($verbose) {
                $this->logStats($total, $index - ($counter * $threshold), 'senses', $console, $tFilter, $nFilter);
                $this->writeSeparator();
                $console->writeLine(
                    'About ' . $nFilter->filter($index) . ' senses written in ' . $tFilter->filter($total) . '.'
                );
                if (count($langs) <= 1) {
                    $this->writeSeparator();
                }
            }
            $time += $total;
            $nsenses += $index;
        }
        if ($verbose && count($langs) > 1) {
            $this->writeSeparator();
            $console->writeLine(
                'A total of ~ ' . $nFilter->filter($nsenses) . ' senses written in ' . $tFilter->filter($time) . '.'
            );
            $this->writeSeparator();
        }

        return false;
    }

    /**
     * // TODO: dedeuplicate language list
     * @param Request $request
     * @return array
     * @throws \Zend\Console\Exception\InvalidArgumentException
     */
    private function getLanguages(Request $request)
    {
        $langs = explode(',', $request->getParam('languages'));
        $configs = $this->getServiceLocator()->get('Config');
        $available_langs = array_keys($configs['languages']);
        foreach ($langs as $l) {
            if (!in_array($l, $available_langs)) {
                throw new InvalidArgumentException("Language \"${l}\" is not supported");
            }
        }

        return $langs;
    }

    /**
     * @param \MongoCollection $collection
     * @param array $query
     * @return \MongoCursor
     */
    private function extractSense(\MongoCollection $collection, $query = [])
    {
        $fields = ['_id' => 1, 'source' => 1, 'synset' => 1, 'type' => 1, 'senses' => 1];

        return $collection->find($query, $fields);
    }

    /**
     * @param $time
     * @param $qty
     * @param $what
     * @param null $console
     * @param null $timeFilter
     * @param null $numFilter
     */
    private function logStats($time, $qty, $what, $console = null, $timeFilter = null, $numFilter = null)
    {
        if ($console === null) {
            $console = $this->getConsole();
        }
        if ($timeFilter === null) {
            $timeFilter = new NumberFormat('en', \NumberFormatter::DURATION);
        }
        if ($numFilter === null) {
            $numFilter = new AbbreviatedNumber(true);
        }
        $console->write($timeFilter->filter($time) . "\t" . $numFilter->filter($qty) . " " . $what . "\n");
    }

    /**
     * @param string $sense
     * @param array $document
     * @param int $index
     * @return array
     */
    private function createSenseData($sense, $document, $index)
    {
        $data = [];
        $exploded = explode(':', $sense);
        $data['region'] = array_shift($exploded);
        array_shift($exploded); // ignore language
        $last = array_pop($exploded);
        preg_match('/^([0-1]*\.?[0-9]+)_([0-9]+)_([0-9]+)/', $last, $matches);
        if ($matches) {
            $numerator = (int)$matches[2];
            $denominator = (int)$matches[3];
            if ($denominator !== 0 && $numerator !== 0) {
                $data['probability'] = (double)$numerator / $denominator;
            } else {
                $data['probability'] = (double)1.0;
            }
            $data['sense'] = implode(':', $exploded);
        } else {
            $data['sense'] = $last;
        }
        $data['type'] = $document['type'];
        $data['synset'] = $document['synset'];
        $data['source'] = $document['source'];
        $data['bnid'] = $document['_id'];
        $data['id'] = $index;

        return $data;
    }

    // TODO check that _, #, : etc. ( will also treated as whitespaces (so not indexed)

    /**
     * @return array|bool
     * @throws InvalidArgumentException
     */
    public function indexAction()
    {
        $console = $this->getConsole();
        $configs = $this->getServiceLocator()->get('Config');
        // Retrieve parameters
        /* @var $request \Zend\Console\Request */
        $request = $this->getRequest();
        $langs = null;
        try {
            $langs = $this->getLanguages($request);
        } catch (InvalidArgumentException $exc) {
            $this->writeHead('Message: "' . $exc->getMessage() . '".', ColorInterface::RED, $console);
            return false;
        }
        // Retrieve SphinxSearch
        /** @var $adapter \Zend\Db\Adapter\Adapter */
        $adapter = $this->getSphinxAdapter(false);
        // Retrieve MongoDB
        /** @var $mongodb \MongoDB */
        $mongodb = $this->getMongoConnection(false);
        //
        $indexer = null;
        $graph = null;
        if ($adapter !== null && $mongodb !== null) {
            $indexer = new Indexer($adapter);
            $graph = $mongodb->selectCollection(ImportController::COREDUMP_COLLECTION);
        } else {
            return false;
        }
        // Drop sphinx tables
        $console->writeLine(str_repeat("=", $console->getWidth()), ColorInterface::GREEN);
        foreach ($langs as $lan) {
            $name = $configs['sphinx']['indexes']['prefix'] . $lan;
            $console->write("Truncating '" . $name . "' real-time index ...\t");
            $adapter->query('TRUNCATE RTINDEX ' . $name, $adapter::QUERY_MODE_EXECUTE);
            $console->write("OK\n", ColorInterface::GREEN);
        }
        $console->writeLine(str_repeat("=", $console->getWidth()), ColorInterface::GREEN);
        // Statistics facilties
        $start = microtime(true);
        $total = null;
        $num = 0; // number of documents
        $index = 0; // number of senses
        $threshold = 100000;
        $counter = 0;
        $numberAbbreviator = new AbbreviatedNumber(true);
        $durationFilter = new NumberFormat('en', \NumberFormatter::DURATION);
        // Start transaction batch indexing
        $indexer->beginTransaction();
        // Fetch babelnet documents (by languages) from mongodb
        foreach ($langs as $lan) {
            $regex = "/^(\w+)\:(" . $lan . ")\:/i";
            $cursor = $this->extractSense($graph, ['senses' => new \MongoRegex($regex)]);
            // Iterate retrieved documents
            foreach ($cursor as $doc) {
                // Filtering senses by current language
                foreach (preg_grep($regex, $doc['senses']) as $sense) {
                    // Log statistics and information
                    if ($index === ($counter * $threshold) + 1) {
                        $console->write("...\t\t\t\t\t\t");
                    }
                    if (($index % $threshold) == 0 && $num > 0) {
                        // Commit indexing transaction and starting a new one
                        $indexer->commit();
                        $indexer->beginTransaction();
                        // Logging chunk indexing statistics
                        $total = microtime(true) - $start;
                        $counter++;
                        $console->write(
                            $durationFilter->filter($total) . "\t" .
                            $numberAbbreviator->filter($counter * $threshold) . " senses\n"
                        );
                    }
                    // Sense extraction
                    $row = $this->createSenseData($sense, $doc, $index);
                    // Sense indexing
                    $indexer->insert('senses_' . $lan, $row);
                    $index++;
                }
                $num++;
            }
        }
        $indexer->commit();
        $total = microtime(true) - $start;
        $console->writeLine(
            $durationFilter->filter($total) . "\t" .
            $numberAbbreviator->filter($index - ($counter * $threshold) + 1) . " senses"
        );
        $console->writeLine(str_repeat('=', $console->getWidth()), ColorInterface::GREEN);
        $console->writeLine(
            "About " . $numberAbbreviator->filter($index) . " senses indexed in " .
            $durationFilter->filter($total) . "."
        );
        $console->writeLine(str_repeat("=", $console->getWidth()), ColorInterface::GREEN);

        return false;
    }
}
