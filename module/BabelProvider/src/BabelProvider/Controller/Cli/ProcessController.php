<?php
/**
 * babel-provider
 *
 * @link        ...
 *
 * @copyright   ...
 *
 * @license     ...
 */
namespace BabelProvider\Controller\Cli;

use BabelProvider\Controller\Cli\CliTrait;
use BabelProvider\Controller\Cli\ConsoleTrait;
use BabelProvider\Filter\StringToConventionalWord;
use Zend\Console\ColorInterface;
use Zend\Console\Request;
use Zend\Mvc\Controller\AbstractActionController;

/**
 * Class ProcessController
 */
class ProcessController extends AbstractActionController
{
    use CliTrait;
    use ConsoleTrait;

    /**
     * @return bool
     */
    public function subsetAction()
    {
        $console = $this->getConsole();
        // Retrieve parameters
        /* @var $request \Zend\Console\Request */
        $request = $this->getRequest();
        $roots = $this->getRoots($request);
        $word = $request->getParam('word');
        $depth = $this->getIntParam('depth');
        $log = $request->getParam('verbose');
        $coll = $request->getParam('collection');
        // Retrieve MongoDB collection
        /** @var $mongodb \MongoDB */
        $mongodb = $this->getMongoConnection(false);
        $graph = null;
        if ($mongodb != null) {
            $graph = $mongodb->selectCollection(ImportController::COREDUMP_COLLECTION);
        }
        // Retrieve word available senses
        $regex = "/.*" . preg_quote(':' . $word, null) . "$/";
        \MongoCursor::$timeout = -1;
        if ($log) {
            $this->writeSeparator();
            $console->writeLine("Word: \t\t" . $word);
            $console->writeLine("Regex: \t\t" . $regex);
            $this->writeSeparator();
        }
        $senses = $graph->find(['senses' => new \MongoRegex($regex)], ['_id' => 1]);
        $identifiers = [];
        foreach ($senses as $sense) {
            $identifiers[] = $sense['_id'];
        }
        // Get the union of root identifiers
        if (!is_null($roots)) {
            $identifiers = array_unique(array_merge($identifiers, $roots));
        }
        // Get hyponyms for each sense found
        $hyponyms = $this->getHyponyms($graph, $identifiers, $depth, null, $log);
        if ($log) {
            $this->writeHead('Total elements found: ' . count($hyponyms));
        }
        // Serialize to MongoDB
        $filter = new StringToConventionalWord();
        $collectionName = $filter->filter($coll);
        $collection = $mongodb->selectCollection($collectionName);
        $collection->drop();
        $collection->insert(['elements' => $hyponyms, 'depth' => $depth]);
        if ($log) {
            $console->writeLine('Domain elements written to MongoDB');
            $console->writeLine('Collection name: "' . $collectionName . '"');
            $this->writeSeparator();
        }

        return false;
    }

    /**
     * @param Request $request
     * @return array
     */
    private function getRoots(Request $request)
    {
        $roots = array_filter(
            array_map(
                function ($value) {
                    if (empty($value)) {
                        return null;
                    }
                    if (preg_match('/^bn:/', $value)) {
                        return $value;
                    } else {
                        return 'bn:' . $value;
                    }
                },
                explode(',', $request->getParam('roots'))
            )
        );
        return $roots;
    }

    /**
     * Reconstruct the graph encoding the hypernymy relations between senses
     *
     * It assumes that the graph is a DAG (directed acyclic graph), so no cycles exists.
     * Otherwise, it loops to infinity (it is recursive).
     *
     * @param \MongoCollection $coll
     * @param $ids
     * @param $depth
     * @param null $level
     * @param bool $log
     * @return array
     */
    private function getHyponyms(\MongoCollection $coll, $ids, $depth, $level = null, $log = false)
    {
        // Retrieve console
        $console = $this->getConsole();
        // Retrieve sematic pointers
        $pointers = $coll->find(['_id' => ['$in' => $ids]]);
        // Retrieve hyponyms and instance hyponyms
        if (is_null($level)) {
            $level = 0;
        }
        $sensen = 1;
        $res = [];
        foreach ($pointers as $pointer) {
            if ($log) {
                if ($level == 0) {
                    $console->writeLine('Sense ' . $sensen, ColorInterface::YELLOW);
                }
                $console->write(
                    str_repeat("\t", $level) .
                    '{' . $pointer['_id'] . '} - ' .
                    '{' . $pointer['source_id'] . '}'
                );
            }
            $identifiers = $this->getHyponymIdentifiers($pointer['rels']);
            $res = array_merge($res, $identifiers);
            $nhypos = count($identifiers);
            if ($nhypos > 0) {
                if ($log) {
                    $console->write(' - ');
                    $console->write($nhypos, ColorInterface::YELLOW);
                    $console->write(' hyponyms' . "\n");
                }
                $next = $level + 1;
                if (is_null($depth)) {
                    $res = array_merge($res, $this->getHyponyms($coll, $identifiers, null, $next, $log));
                } elseif ($depth > 0) {
                    $res = array_merge($res, $this->getHyponyms($coll, $identifiers, $depth - 1, $next, $log));
                }
            } elseif ($log) {
                $console->write(' - no hyponyms' . "\n");
            }
            $sensen++;
        }

        return $res;
    }

    /**
     * @param array $pointers
     * @return array
     */
    private function getHyponymIdentifiers(array $pointers)
    {
        $hypos = preg_grep('/^~(i)?\\sbn:.*$/', $pointers);
        return preg_filter('/(.*)\\s(bn:.*)$/', '$2', $hypos);
    }
}
