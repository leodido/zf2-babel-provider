<?php
/**
 * babel-provider
 *
 * @link        ...
 *
 * @copyright   ...
 *
 * @license     ...
 */
namespace BabelProvider\Model\Sphinx;

/**
 * Class XMLSource2
 * Efficiently generate XML for Sphinx's xmlpipe2 data adapter
 *
 * @author leodido
 */
class XMLSource2 extends \XMLWriter
{
    private $fields = [];
    private $attributes = [];
    private $knownFieldAttrs = [
        'name' => null,
        'attr' => '/(string|wordcount)/'
    ];
    private $knownAttributeAttrs = [
        'name' => null,
        'type' => '/(int|timestamp|str2ordinal|bool|float|multi|string)/',
        'bits' => '/^[1-2][0-9]$|^[3][0-2]$|^[1-9]{1,1}$/', // range from 1 to 32
        'default' => null
    ];

    /**
     * @param array $options
     */
    public function __construct($options = [])
    {
        $defaults = ['indent' => false];
        $options = array_merge($defaults, $options);
        // Store the xml tree in memory
        $this->openMemory();
        if ($options['indent']) {
            $this->setIndent(true);
        }
    }

    /**
     * @param $fields
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
    }

    /**
     * @param $attributes
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * @param $doc
     */
    public function addDocument($doc)
    {
        $this->startElement('sphinx:document');
        $this->writeAttribute('id', $doc['id']);
        foreach ($doc as $key => $value) {
            // Skip the id key since that is an element attribute
            if ($key == 'id') {
                continue;
            }
            $this->startElement($key);
            $this->text($value);
            $this->endElement();
        }
        $this->endElement();
        print $this->outputMemory();
    }

    public function beginOutput()
    {
        $this->startDocument('1.0', 'UTF-8');
        $this->startElement('sphinx:docset');
        $this->startElement('sphinx:schema');
        // Add fields to the schema
        foreach ($this->fields as $fields) {
            $this->startElement('sphinx:field');
            foreach ($fields as $key => $value) {
                if (in_array($key, array_keys($this->knownFieldAttrs))) {
                    $constraint = $this->knownFieldAttrs[$key];
                    if ($constraint === null || preg_match($constraint, $value)) {
                        $this->writeAttribute($key, $value);
                    }
                } // FIXME: throw an exception otherwise?
            }
            $this->endElement();
        }
        // Add attributes to the schema
        foreach ($this->attributes as $attributes) {
            $this->startElement('sphinx:attr');
            foreach ($attributes as $key => $value) {
                if (in_array($key, array_keys($this->knownAttributeAttrs))) {
                    $constraint = $this->knownAttributeAttrs[$key];
                    if ($constraint === null || preg_match($constraint, $value)) {
                        $this->writeAttribute($key, $value);
                    }
                } // FIXME: throw an exception otherwise?
            }
            $this->endElement();
        }
        // End sphinx:schema
        $this->endElement();
        print $this->outputMemory();
    }

    public function endOutput()
    {
        // End sphinx:docset
        $this->endElement();
        print $this->outputMemory();
    }
}
