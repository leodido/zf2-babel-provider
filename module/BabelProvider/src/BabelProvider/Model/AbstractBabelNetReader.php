<?php
/**
 * babel-provider
 *
 * @link        ...
 *
 * @copyright   ...
 *
 * @license     ...
 */

namespace BabelProvider\Model;

/**
 * Class AbstractBabelNetReader
 */
abstract class AbstractBabelNetReader
{
    /**
     * @var resource
     */
    protected $handle;

    /**
     * @param $file
     * @throws \Exception
     */
    public function __construct($file)
    {
        $handle = fopen($file, 'r');
        if ($handle) {
            $this->handle = $handle;
        } else {
            throw new \Exception('Unable to read ' . $file);
        }
    }

    public function __destruct()
    {
        fclose($this->handle);
    }

    /**
     * @return \Generator
     */
    abstract public function getLines();
}
