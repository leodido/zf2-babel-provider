<?php
/**
 * babel-provider
 *
 * @link        ...
 *
 * @copyright   ...
 *
 * @license     ...
 */

namespace BabelProvider\Model;

/**
 * Class BabelNetGlossesReader
 */
class BabelNetGlossesReader extends AbstractBabelNetReader
{
    /**
     * @return \Generator
     */
    public function getLines()
    {
        // Skip comments
        // NOTE: it assumes that comments exists only in the head lines
        while (strpos($line = fgets($this->handle), '%') === 0) {
            ;
        }
        $tmp = [];
        do {
            if ($line == "\n") {
                yield $this->processLine($tmp);
                $tmp = [];
            } else {
                $tmp[] = trim($line);
            }
        } while ($line = fgets($this->handle));
    }

    /**
     * @param $line
     * @return array
     * @throws \RuntimeException
     */
    protected function processLine($line)
    {
        $return = [];
        $first = array_shift($line);
        if (strpos($first, 'bn:') === 0) {
            $return['_id'] = $first;
            foreach ($line as $num => $gloss) {
                $info = explode("\t", $gloss);
                if (count($info) !== 4) {
                    throw new \RuntimeException("Malformed information detected.");
                }
                $key = $info[1] . ":" . $info[0]; // TODO: understand what WIKI:SIMPLE means
                $return[$key][] = [
                    $info[2] => $info[3]
                ];
            }
        } else {
            throw new \RuntimeException("BabelNet synset id (e.g., \"bn:XXXXXXXXZ\") not detected.");
        }

        return $return;
    }
}
