<?php
namespace BabelProvider\Model;

/**
 * Class BabelNetCoreReader
 */
class BabelNetCoreReader extends AbstractBabelNetReader
{
    /**
     * @return \Generator
     */
    public function getLines()
    {
        // Skip comments
        // NOTE: it assumes that comments exists only in the head lines
        while (strpos($line = fgets($this->handle), '%') === 0) {
            ;
        }

        do {
            yield $this->processLine($line);
        } while ($line = fgets($this->handle));
    }

    /**
     * @param $line
     * @return array
     */
    protected function processLine($line)
    {
        $parts = explode("\t", $line);
        $return = [
            '_id' => $parts[0],
            'synset' => substr($parts[0], -1, 1),
            'source' => $parts[1],
            'source_id' => $parts[2],
            'type' => $parts[3],
        ];
        // Senses [D]
        $count = 4;
        $return['n_senses'] = (int)$parts[$count];
        $return['senses'] = array_slice($parts, $count + 1, $return['n_senses']);
        $count += 1 + $return['n_senses'];
        // Translation mappings [E]
        // NOTE: only mapping about automatic (so, not 100% sure) translations are provided
        $return['n_trans'] = (int)$parts[$count];
        $count++;
        if ($return['n_trans'] > 0) {
            $trans = [];
            // NOTE: check that $parts contains '_'
            // NOTE: if there is only one translation mapping $parts does not contains ',' character
            while (strpos($parts[$count], '_') !== false) {
                $trans = $parts[$count];
                $count++;
            }
            $return['trans'] = $trans;
        }
        // Assets
        $return['n_assets'] = (int)$parts[$count];
        if ($return['n_assets'] > 0) {
            $return['assets'] = array_slice($parts, $count + 1, $return['n_assets']);
        }
        $count += 1 + $return['n_assets'];
        // Categories
        $return['n_cats'] = (int)$parts[$count];
        if ($return['n_cats'] > 0) {
            $return['cats'] = array_slice($parts, $count + 1, $return['n_cats']);
        }
        $count += 1 + $return['n_cats'];
        // Semantic pointers [F]
        $return['n_rels'] = (int)$parts[$count];
        if ($return['n_rels'] > 0) {
            $return['rels'] = array_slice($parts, $count + 1, $return['n_rels']);
        }
//      NOTE: semantic pointers listed at http://wordnet.princeton.edu/wordnet/man/wninput.5WN.html
//      NOTE: regex to catch bn relations is ([\\\$\^\*\+\-!@~#%=;><&][imspcru]?|r|gmono|gdis)\sbn:\d+[a-z](\|\w+_\w+)?

        return $return;
    }
}
