<?php
/**
 * babel-provider
 *
 * @link        ...
 *
 * @copyright   ...
 *
 * @license     ...
 */

return [
    'console' => [
        'router' => [
            'routes' => [
                'babelnet-test' => [
                    'options' => [
                        'route' => 'test',
                        'defaults' => [
                            '__NAMESPACE__' => 'BabelProvider\Controller\Cli',
                            'controller' => 'import',
                            'action' => 'test'
                        ]
                    ]
                ],
                'babelnet-import' => [
                    'options' => [
                        'route' => 'import',
                        'defaults' => [
                            '__NAMESPACE__' => 'BabelProvider\Controller\Cli',
                            'controller' => 'import',
                            'action' => 'import'
                        ]
                    ]
                ],
                'babelnet-provide' => [
                    'options' => [
                        'route' => 'provide --dump= [--glosses=]',
                        'defaults' => [
                            '__NAMESPACE__' => 'BabelProvider\Controller\Cli',
                            'controller' => 'import',
                            'action' => 'provide'
                        ]
                    ]
                ],
                'babelnet-subset' => [
                    'options' => [
                        'route' => 'subset --word= --collection= [--depth=] [--roots=] [--verbose]',
                        'defaults' => [
                            '__NAMESPACE__' => 'BabelProvider\Controller\Cli',
                            'controller' => 'process',
                            'action' => 'subset'
                        ]
                    ]
                ],
                'sphinx-plain' => [
                    'options' => [
                        'route' => 'create source --languages= [--stdout] [--verbose]',
                        'defaults' => [
                            '__NAMESPACE__' => 'BabelProvider\Controller\Cli',
                            'controller' => 'sphinx',
                            'action' => 'create-source'
                        ]
                    ]
                ],
                'sphinx-rt' => [
                    'options' => [
                        'route' => 'index --languages=',
                        'defaults' => [
                            '__NAMESPACE__' => 'BabelProvider\Controller\Cli',
                            'controller' => 'sphinx',
                            'action' => 'index'
                        ]
                    ]
                ],
            ]
        ]
    ],
    'service_manager' => [
        'abstract_factories' => [
            'Mongovc\Service\MongoDbAbstractServiceFactory'
        ],
        'factories' => [
            'SphinxSearch\Db\Adapter\Adapter' => 'SphinxSearch\Db\Adapter\AdapterServiceFactory'
        ],
        'aliases' => [
            'sphinxql' => 'SphinxSearch\Db\Adapter\Adapter'
        ]
    ],
    'controllers' => [
        'invokables' => [
            'BabelProvider\Controller\Cli\Import' => 'BabelProvider\Controller\Cli\ImportController',
            'BabelProvider\Controller\Cli\Sphinx' => 'BabelProvider\Controller\Cli\SphinxController',
            'BabelProvider\Controller\Cli\Process' => 'BabelProvider\Controller\Cli\ProcessController'
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => false
    ]
];
