Babel Provider
==============

A ZF2 module that provides console actions to process and import BabelNet core dumps into MongoDB and Sphinx Search.

## Dependecies

* PHP >= 5.5
* Composer
* Docker
* MongoDB
* Sphinx Search docker container (i.e., [leodido/sphinxsearch](https://github.com/leodido/docker-sphinxsearch))

## Usage

1. Enter in the root directory

2. Provide the core dump file (and, optionally, also the glosses file)

    `./bin/babel-provider provide --dump=/data/bn-2.0-core-dump --glosses=/data/bn-2.0-glosses`

3. Parse the core dump and store extracted data into MongoDB

    `./bin/babel-provider import`

4. Store the extracted senses into Sphinx Search indexes by language (see [next section](#indexing))

### Indexing

The indexing of the BabelNet senses can be performed in two ways: with real-time indexes or with plain indexes.

#### Real-time indexes

1. Create a Sphinx Search configuration and start it via `./bin/sphinx-rt-index` script on the fly

    E.g., `./bin/sphinx-rt-index -l it,en,de`

2. Index the senses into real-time indexes

    E.g., `./bin/babel-provider index --languages=it,en,de`

##### Note

This indexing method is **very slow**. Searching performances are **fast enough** in optimal situations (e.g., `rt_mem_limit`).

#### Plain indexes

1. Create the Sphinx Search XML data sources

    E.g., `./bin/babel-provider create source --languages=it,en,de --verbose`

2. Create a Sphinx Search configuration and index the senses into plain indexes

    E.g., `./bin/sphinx-pl-index -l it,en,de`

##### Note

This indexing method is **very fast**. Also searching performances are **very fast**.

Note that this type of indexes are **static**: in presence of new data they need to be completely re-indexed (or updated with delta indexes).

### Domain extraction

You can extract the set of hyponyms of a given concept.

`./bin/babel-provider subset --word=food --collection=fooddomain --verbose`

This command save the set (a plain list of BabelNet identifiers) to a MongoDB collection named `food`.

### Support actions

To test that application connects, you can simply execute:

`./bin/babel-provider test`

If everything is okay it should print:

```
> MongoDB ...           OK
> SphinxSearch ...      OK
```
