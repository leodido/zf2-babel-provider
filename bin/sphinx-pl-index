#!/bin/bash
#
# babel-provider
# @link        ...
# @copyright   ...
# @license     ...

usage() {
    printf "\033[1mUsage: sphinx-pl-index [options]\033[0m
This script creates a configuration of \033[1mSphinx Search\033[0m plain indexes and index them.
The Sphinx Search configuration template is located at \033[1m\"../config/sphinx/template.pl.conf\"\033[0m file.

OPTIONS:
    -l    The list of languages ​​for which replicate the template index.
    -h    Shows this help.
EXAMPLES:
    sphinx-pl-index -l it,en,de,es,fr,pt
"
}

LANGUAGES=
while getopts ":l::h" OPTION; do
    case $OPTION in
        l)
            LANGUAGES=${OPTARG}
            ;;
        \?)
            echo "Invalid option: -${OPTARG}" >&2
            exit 1
            ;;
        h)
            usage
            exit 1
            ;;
    esac
done
if [ -z $LANGUAGES ]; then
    usage
    exit 1
fi

cd "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Config directory
ROOTDIR=`readlink -e ${PWD}/..`
CONFIGDIR=${ROOTDIR}/config/sphinx

# Create config file
LANGUAGES=${LANGUAGES} php -f ${CONFIGDIR}/template.pl.conf > ${CONFIGDIR}/sphinx.conf

# Create launch command
INDEXER="docker run -i -t -v ${CONFIGDIR}:/usr/local/etc -p 127.0.0.1:9306:9306 -d leodido/sphinxsearch:2.2.2-beta ./indexall.sh"
echo -e "\033[1m> LAUNCHING COMMAND:\033[0m\n\033[1m> ${INDEXER}\033[0m"
PID=$($INDEXER)
echo -e "\033[1m> PID: ${PID}\033[0m\n"
