<?php
/**
 * babel-provider
 *
 * @link        ...
 *
 * @copyright   ...
 *
 * @license     ...
 */

chdir(dirname(__DIR__));

if ($argc < 2) {
    echo "Usage:  {$argv[0]} <module> ...\n";
    exit(0);
}

$module = $argv[1];

$configFile = "config/{$module}.config.php";

--$argc;

unset($argv[1]);
$argv = array_values($argv);
$_SERVER['argv'] = $argv;

if (!file_exists($configFile)) {
    echo "Could not load configuration for module {$module}\n";
    exit(1);
}

require 'init_autoloader.php';

Zend\Mvc\Application::init(require $configFile)->run();